<?xml version="1.0" ?>
<sld:StyledLayerDescriptor version="1.0.0" xmlns="http://www.opengis.net/sld" xmlns:gml="http://www.opengis.net/gml" xmlns:ogc="http://www.opengis.net/ogc" xmlns:sld="http://www.opengis.net/sld">
    <sld:UserLayer>
        <sld:LayerFeatureConstraints>
            <sld:FeatureTypeConstraint/>
        </sld:LayerFeatureConstraints>
        <sld:UserStyle>
            <sld:Name>dem_lx</sld:Name>
            <sld:Title/>
            <sld:FeatureTypeStyle>
                <sld:Name/>
                <sld:Rule>
                    <sld:RasterSymbolizer>
                        <sld:Geometry>
                            <ogc:PropertyName>grid</ogc:PropertyName>
                        </sld:Geometry>
                        <sld:Opacity>1</sld:Opacity>
                        <sld:ColorMap>
                            <sld:ColorMapEntry color="#aeefd5" label="13" opacity="1.0" quantity="13"/>
                            <sld:ColorMapEntry color="#aff0d3" label="14.3" opacity="1.0" quantity="14.3"/>
                            <sld:ColorMapEntry color="#b0f2d0" label="15.6" opacity="1.0" quantity="15.6"/>
                            <sld:ColorMapEntry color="#b0f2ca" label="16.9" opacity="1.0" quantity="16.9"/>
                            <sld:ColorMapEntry color="#b1f2c4" label="18.3" opacity="1.0" quantity="18.3"/>
                            <sld:ColorMapEntry color="#b0f3be" label="19.6" opacity="1.0" quantity="19.6"/>
                            <sld:ColorMapEntry color="#b0f4ba" label="20.9" opacity="1.0" quantity="20.9"/>
                            <sld:ColorMapEntry color="#b2f6b5" label="22.2" opacity="1.0" quantity="22.2"/>
                            <sld:ColorMapEntry color="#b5f6b2" label="23.5" opacity="1.0" quantity="23.5"/>
                            <sld:ColorMapEntry color="#baf7b2" label="24.8" opacity="1.0" quantity="24.8"/>
                            <sld:ColorMapEntry color="#c0f7b2" label="26.1" opacity="1.0" quantity="26.1"/>
                            <sld:ColorMapEntry color="#c6f8b2" label="27.4" opacity="1.0" quantity="27.4"/>
                            <sld:ColorMapEntry color="#ccf9b2" label="28.8" opacity="1.0" quantity="28.8"/>
                            <sld:ColorMapEntry color="#d2fab1" label="30.1" opacity="1.0" quantity="30.1"/>
                            <sld:ColorMapEntry color="#d9fab2" label="31.4" opacity="1.0" quantity="31.4"/>
                            <sld:ColorMapEntry color="#e0fbb2" label="32.7" opacity="1.0" quantity="32.7"/>
                            <sld:ColorMapEntry color="#e7fcb2" label="34" opacity="1.0" quantity="34"/>
                            <sld:ColorMapEntry color="#eefcb3" label="35.3" opacity="1.0" quantity="35.3"/>
                            <sld:ColorMapEntry color="#f5fcb3" label="36.6" opacity="1.0" quantity="36.6"/>
                            <sld:ColorMapEntry color="#fafcb2" label="38" opacity="1.0" quantity="38"/>
                            <sld:ColorMapEntry color="#f8f9ac" label="39.3" opacity="1.0" quantity="39.3"/>
                            <sld:ColorMapEntry color="#eef4a2" label="40.6" opacity="1.0" quantity="40.6"/>
                            <sld:ColorMapEntry color="#e2f097" label="41.9" opacity="1.0" quantity="41.9"/>
                            <sld:ColorMapEntry color="#d5eb8c" label="43.2" opacity="1.0" quantity="43.2"/>
                            <sld:ColorMapEntry color="#c6e480" label="44.5" opacity="1.0" quantity="44.5"/>
                            <sld:ColorMapEntry color="#b8de76" label="45.8" opacity="1.0" quantity="45.8"/>
                            <sld:ColorMapEntry color="#aad86c" label="47.1" opacity="1.0" quantity="47.1"/>
                            <sld:ColorMapEntry color="#9ad362" label="48.5" opacity="1.0" quantity="48.5"/>
                            <sld:ColorMapEntry color="#8ccd59" label="49.8" opacity="1.0" quantity="49.8"/>
                            <sld:ColorMapEntry color="#7dc752" label="51.1" opacity="1.0" quantity="51.1"/>
                            <sld:ColorMapEntry color="#6ec24a" label="52.4" opacity="1.0" quantity="52.4"/>
                            <sld:ColorMapEntry color="#5ebc42" label="53.7" opacity="1.0" quantity="53.7"/>
                            <sld:ColorMapEntry color="#4db639" label="55" opacity="1.0" quantity="55"/>
                            <sld:ColorMapEntry color="#3eb032" label="56.3" opacity="1.0" quantity="56.3"/>
                            <sld:ColorMapEntry color="#31ab2c" label="57.7" opacity="1.0" quantity="57.7"/>
                            <sld:ColorMapEntry color="#27a52a" label="59" opacity="1.0" quantity="59"/>
                            <sld:ColorMapEntry color="#1ea02b" label="60.3" opacity="1.0" quantity="60.3"/>
                            <sld:ColorMapEntry color="#189a2e" label="61.6" opacity="1.0" quantity="61.6"/>
                            <sld:ColorMapEntry color="#129431" label="62.9" opacity="1.0" quantity="62.9"/>
                            <sld:ColorMapEntry color="#0e8e34" label="64.2" opacity="1.0" quantity="64.2"/>
                            <sld:ColorMapEntry color="#098938" label="65.5" opacity="1.0" quantity="65.5"/>
                            <sld:ColorMapEntry color="#07843c" label="66.8" opacity="1.0" quantity="66.8"/>
                            <sld:ColorMapEntry color="#0c823f" label="68.2" opacity="1.0" quantity="68.2"/>
                            <sld:ColorMapEntry color="#18823f" label="69.5" opacity="1.0" quantity="69.5"/>
                            <sld:ColorMapEntry color="#28843d" label="70.8" opacity="1.0" quantity="70.8"/>
                            <sld:ColorMapEntry color="#34883c" label="72.1" opacity="1.0" quantity="72.1"/>
                            <sld:ColorMapEntry color="#408c3b" label="73.4" opacity="1.0" quantity="73.4"/>
                            <sld:ColorMapEntry color="#4c8e3b" label="74.7" opacity="1.0" quantity="74.7"/>
                            <sld:ColorMapEntry color="#579238" label="76" opacity="1.0" quantity="76"/>
                            <sld:ColorMapEntry color="#639436" label="77.4" opacity="1.0" quantity="77.4"/>
                            <sld:ColorMapEntry color="#6e9634" label="78.7" opacity="1.0" quantity="78.7"/>
                            <sld:ColorMapEntry color="#789a32" label="80" opacity="1.0" quantity="80"/>
                            <sld:ColorMapEntry color="#809c30" label="81.3" opacity="1.0" quantity="81.3"/>
                            <sld:ColorMapEntry color="#89a02e" label="82.6" opacity="1.0" quantity="82.6"/>
                            <sld:ColorMapEntry color="#93a22b" label="83.9" opacity="1.0" quantity="83.9"/>
                            <sld:ColorMapEntry color="#9ca429" label="85.2" opacity="1.0" quantity="85.2"/>
                            <sld:ColorMapEntry color="#a6a627" label="86.5" opacity="1.0" quantity="86.5"/>
                            <sld:ColorMapEntry color="#b0aa24" label="87.9" opacity="1.0" quantity="87.9"/>
                            <sld:ColorMapEntry color="#bbad22" label="89.2" opacity="1.0" quantity="89.2"/>
                            <sld:ColorMapEntry color="#c5b01e" label="90.5" opacity="1.0" quantity="90.5"/>
                            <sld:ColorMapEntry color="#cfb11c" label="91.8" opacity="1.0" quantity="91.8"/>
                            <sld:ColorMapEntry color="#dab318" label="93.1" opacity="1.0" quantity="93.1"/>
                            <sld:ColorMapEntry color="#e4b414" label="94.4" opacity="1.0" quantity="94.4"/>
                            <sld:ColorMapEntry color="#eeb60e" label="95.7" opacity="1.0" quantity="95.7"/>
                            <sld:ColorMapEntry color="#f6b608" label="97.1" opacity="1.0" quantity="97.1"/>
                            <sld:ColorMapEntry color="#f8b004" label="98.4" opacity="1.0" quantity="98.4"/>
                            <sld:ColorMapEntry color="#f4a602" label="99.7" opacity="1.0" quantity="99.7"/>
                            <sld:ColorMapEntry color="#ee9b02" label="101" opacity="1.0" quantity="101"/>
                            <sld:ColorMapEntry color="#e89002" label="102" opacity="1.0" quantity="102"/>
                            <sld:ColorMapEntry color="#e28402" label="104" opacity="1.0" quantity="104"/>
                            <sld:ColorMapEntry color="#dc7a02" label="105" opacity="1.0" quantity="105"/>
                            <sld:ColorMapEntry color="#d86f02" label="106" opacity="1.0" quantity="106"/>
                            <sld:ColorMapEntry color="#d36602" label="108" opacity="1.0" quantity="108"/>
                            <sld:ColorMapEntry color="#ce5c02" label="109" opacity="1.0" quantity="109"/>
                            <sld:ColorMapEntry color="#c85402" label="110" opacity="1.0" quantity="110"/>
                            <sld:ColorMapEntry color="#c04a02" label="112" opacity="1.0" quantity="112"/>
                            <sld:ColorMapEntry color="#ba4202" label="113" opacity="1.0" quantity="113"/>
                            <sld:ColorMapEntry color="#b43a02" label="114" opacity="1.0" quantity="114"/>
                            <sld:ColorMapEntry color="#ae3102" label="115" opacity="1.0" quantity="115"/>
                            <sld:ColorMapEntry color="#a92a02" label="117" opacity="1.0" quantity="117"/>
                            <sld:ColorMapEntry color="#a32402" label="118" opacity="1.0" quantity="118"/>
                            <sld:ColorMapEntry color="#9d1e02" label="119" opacity="1.0" quantity="119"/>
                            <sld:ColorMapEntry color="#971702" label="121" opacity="1.0" quantity="121"/>
                            <sld:ColorMapEntry color="#921201" label="122" opacity="1.0" quantity="122"/>
                            <sld:ColorMapEntry color="#8d0e01" label="123" opacity="1.0" quantity="123"/>
                            <sld:ColorMapEntry color="#870800" label="125" opacity="1.0" quantity="125"/>
                            <sld:ColorMapEntry color="#820500" label="126" opacity="1.0" quantity="126"/>
                            <sld:ColorMapEntry color="#7d0400" label="127" opacity="1.0" quantity="127"/>
                            <sld:ColorMapEntry color="#7a0802" label="129" opacity="1.0" quantity="129"/>
                            <sld:ColorMapEntry color="#770d02" label="130" opacity="1.0" quantity="130"/>
                            <sld:ColorMapEntry color="#761002" label="131" opacity="1.0" quantity="131"/>
                            <sld:ColorMapEntry color="#751204" label="133" opacity="1.0" quantity="133"/>
                            <sld:ColorMapEntry color="#751404" label="134" opacity="1.0" quantity="134"/>
                            <sld:ColorMapEntry color="#751504" label="135" opacity="1.0" quantity="135"/>
                            <sld:ColorMapEntry color="#741604" label="136" opacity="1.0" quantity="136"/>
                            <sld:ColorMapEntry color="#741805" label="138" opacity="1.0" quantity="138"/>
                            <sld:ColorMapEntry color="#721a06" label="139" opacity="1.0" quantity="139"/>
                            <sld:ColorMapEntry color="#721d06" label="140" opacity="1.0" quantity="140"/>
                            <sld:ColorMapEntry color="#701f07" label="142" opacity="1.0" quantity="142"/>
                            <sld:ColorMapEntry color="#6f2108" label="143" opacity="1.0" quantity="143"/>
                            <sld:ColorMapEntry color="#6e2308" label="144" opacity="1.0" quantity="144"/>
                            <sld:ColorMapEntry color="#6e2408" label="146" opacity="1.0" quantity="146"/>
                            <sld:ColorMapEntry color="#6d2609" label="147" opacity="1.0" quantity="147"/>
                            <sld:ColorMapEntry color="#6c280a" label="148" opacity="1.0" quantity="148"/>
                            <sld:ColorMapEntry color="#6c280a" label="150" opacity="1.0" quantity="150"/>
                            <sld:ColorMapEntry color="#6c2a0a" label="151" opacity="1.0" quantity="151"/>
                            <sld:ColorMapEntry color="#6b2c0b" label="152" opacity="1.0" quantity="152"/>
                            <sld:ColorMapEntry color="#6a2c0c" label="154" opacity="1.0" quantity="154"/>
                            <sld:ColorMapEntry color="#6a2e0c" label="155" opacity="1.0" quantity="155"/>
                            <sld:ColorMapEntry color="#6b300e" label="156" opacity="1.0" quantity="156"/>
                            <sld:ColorMapEntry color="#6e3412" label="157" opacity="1.0" quantity="157"/>
                            <sld:ColorMapEntry color="#713917" label="159" opacity="1.0" quantity="159"/>
                            <sld:ColorMapEntry color="#743e1c" label="160" opacity="1.0" quantity="160"/>
                            <sld:ColorMapEntry color="#764220" label="161" opacity="1.0" quantity="161"/>
                            <sld:ColorMapEntry color="#794625" label="163" opacity="1.0" quantity="163"/>
                            <sld:ColorMapEntry color="#7d4a2b" label="164" opacity="1.0" quantity="164"/>
                            <sld:ColorMapEntry color="#804f32" label="165" opacity="1.0" quantity="165"/>
                            <sld:ColorMapEntry color="#835538" label="167" opacity="1.0" quantity="167"/>
                            <sld:ColorMapEntry color="#875a3f" label="168" opacity="1.0" quantity="168"/>
                            <sld:ColorMapEntry color="#8a6045" label="169" opacity="1.0" quantity="169"/>
                            <sld:ColorMapEntry color="#8c654c" label="171" opacity="1.0" quantity="171"/>
                            <sld:ColorMapEntry color="#906a54" label="172" opacity="1.0" quantity="172"/>
                            <sld:ColorMapEntry color="#936f5a" label="173" opacity="1.0" quantity="173"/>
                            <sld:ColorMapEntry color="#967460" label="175" opacity="1.0" quantity="175"/>
                            <sld:ColorMapEntry color="#987a68" label="176" opacity="1.0" quantity="176"/>
                            <sld:ColorMapEntry color="#9c8170" label="177" opacity="1.0" quantity="177"/>
                            <sld:ColorMapEntry color="#9e8778" label="178" opacity="1.0" quantity="178"/>
                            <sld:ColorMapEntry color="#a08d82" label="180" opacity="1.0" quantity="180"/>
                            <sld:ColorMapEntry color="#a3938b" label="181" opacity="1.0" quantity="181"/>
                            <sld:ColorMapEntry color="#a69a93" label="182" opacity="1.0" quantity="182"/>
                            <sld:ColorMapEntry color="#a7a09c" label="184" opacity="1.0" quantity="184"/>
                            <sld:ColorMapEntry color="#aaa7a4" label="185" opacity="1.0" quantity="185"/>
                            <sld:ColorMapEntry color="#acacab" label="186" opacity="1.0" quantity="186"/>
                            <sld:ColorMapEntry color="#aeaeae" label="188" opacity="1.0" quantity="188"/>
                            <sld:ColorMapEntry color="#b2b2b2" label="189" opacity="1.0" quantity="189"/>
                            <sld:ColorMapEntry color="#b5b5b5" label="190" opacity="1.0" quantity="190"/>
                            <sld:ColorMapEntry color="#b8b8b8" label="192" opacity="1.0" quantity="192"/>
                            <sld:ColorMapEntry color="#bcbcbc" label="193" opacity="1.0" quantity="193"/>
                            <sld:ColorMapEntry color="#c0c0c0" label="194" opacity="1.0" quantity="194"/>
                            <sld:ColorMapEntry color="#c4c4c4" label="196" opacity="1.0" quantity="196"/>
                            <sld:ColorMapEntry color="#c8c8c8" label="197" opacity="1.0" quantity="197"/>
                            <sld:ColorMapEntry color="#cccccc" label="198" opacity="1.0" quantity="198"/>
                            <sld:ColorMapEntry color="#d0ced0" label="199" opacity="1.0" quantity="199"/>
                            <sld:ColorMapEntry color="#d4d2d4" label="201" opacity="1.0" quantity="201"/>
                            <sld:ColorMapEntry color="#d8d6d8" label="202" opacity="1.0" quantity="202"/>
                            <sld:ColorMapEntry color="#dad8da" label="203" opacity="1.0" quantity="203"/>
                            <sld:ColorMapEntry color="#dddbdd" label="205" opacity="1.0" quantity="205"/>
                            <sld:ColorMapEntry color="#e1dfe1" label="206" opacity="1.0" quantity="206"/>
                            <sld:ColorMapEntry color="#e5e3e5" label="207" opacity="1.0" quantity="207"/>
                            <sld:ColorMapEntry color="#e9e7e9" label="209" opacity="1.0" quantity="209"/>
                            <sld:ColorMapEntry color="#ebe9eb" label="210" opacity="1.0" quantity="210"/>
                        </sld:ColorMap>
                    </sld:RasterSymbolizer>
                </sld:Rule>
            </sld:FeatureTypeStyle>
        </sld:UserStyle>
    </sld:UserLayer>
</sld:StyledLayerDescriptor>
